 export function draw(ctx){
   // Quadratic curves example
   ctx.beginPath();
   ctx.moveTo(75, 25);
   ctx.quadraticCurveTo(25, 25, 25, 62.5);
   ctx.quadraticCurveTo(25, 100, 50, 100);
   ctx.quadraticCurveTo(50, 120, 30, 125);
   ctx.quadraticCurveTo(60, 120, 65, 100);
   ctx.quadraticCurveTo(125, 100, 125, 62.5);
   ctx.quadraticCurveTo(125, 25, 75, 25);
   ctx.stroke();
}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#bezier_and_quadratic_curves"