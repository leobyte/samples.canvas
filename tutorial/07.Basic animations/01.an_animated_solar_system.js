let sun = null;
let moon = null;
let earth = null;
let ctx = null;
let aid = null;

export async function draw(_ctx) {
  if(!ctx)
    ctx = _ctx;
  sun =  await Graphics.Image.load(__DIR__+"/canvas_sun.png");
  moon = await Graphics.Image.load(__DIR__+"/canvas_moon.png");
  earth = await Graphics.Image.load(__DIR__+"/canvas_earth.png");

  aid = window.requestAnimationFrame(drawAnimation);

}

function drawAnimation() {

  ctx.globalCompositeOperation = 'destination-over';
  ctx.clearRect(0, 0, 300, 300); // clear canvas

  ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
  ctx.strokeStyle = 'rgba(0, 153, 255, 0.4)';
  ctx.save();
  ctx.translate(150, 150);

  // Earth
  const time = new Date();
  ctx.rotate(((2 * Math.PI) / 60) * time.getSeconds() + ((2 * Math.PI) / 60000) * time.getMilliseconds());
  ctx.translate(105, 0);
  ctx.fillRect(0, -12, 40, 24); // Shadow
  ctx.drawImage(earth, -12, -12);

  // Moon
  ctx.save();
  ctx.rotate(((2 * Math.PI) / 6) * time.getSeconds() + ((2 * Math.PI) / 6000) * time.getMilliseconds());
  ctx.translate(0, 28.5);
  ctx.drawImage(moon, -3.5, -3.5);
  ctx.restore();

  ctx.restore();

  ctx.beginPath();
  ctx.arc(150, 150, 105, 0, Math.PI * 2, false); // Earth orbit
  ctx.stroke();

  ctx.drawImage(sun, 0, 0, 300, 300);

  aid = window.requestAnimationFrame(drawAnimation);
}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations#an_animated_solar_system";
draw.img = "01.an_animated_solar_system.gif";
draw.clear = function(){
  window.cancelAnimationFrame(aid);
}