export function draw(ctx) {
  /* MDN
  // create new image object to use as pattern
  const img = new Image();
  img.src = 'canvas_createpattern.png';
  img.onload = () => {
    // create pattern
    const ptrn = ctx.createPattern(img, 'repeat');
    ctx.fillStyle = ptrn;
    ctx.fillRect(0, 0, 150, 150);
  }*/

  /* sciter*/ 
  Graphics.Image.load(__DIR__+"/canvas_createpattern.png").then(img=>{
    const ptrn = ctx.createPattern(img, 'repeat');
    ctx.fillStyle = ptrn;
    ctx.fillRect(0, 0, 150, 150);
  }).catch(err=>{
    ctx.fillText('TBD', 5, 30);
    console.log(err.message);
  })

}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Applying_styles_and_colors#a_createpattern_example"