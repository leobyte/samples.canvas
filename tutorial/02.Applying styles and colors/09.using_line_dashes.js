let offset = 0;
let _ctx = null;
function march(ctx){
  ctx.clearRect();
  ctx.setLineDash([4, 2]);
  ctx.lineDashOffset = -offset;
  ctx.strokeRect(10, 10, 100, 100);
}

export function draw(ctx) {
  if(ctx)
    _ctx = ctx;
  offset++;
  if (offset > 16) {
    offset = 0;
  }
  march(_ctx);
}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Applying_styles_and_colors#using_line_dashes"
draw.img="09.using_line_dashes.gif"