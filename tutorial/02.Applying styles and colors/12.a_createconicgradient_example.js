export function draw(ctx) {
  
    // Create gradients
    const conicGrad1 = ctx.createConicGradient(2, 62, 75);
    conicGrad1.addColorStop(0, '#A7D30C');
    conicGrad1.addColorStop(1, '#fff');

    const conicGrad2 = ctx.createConicGradient(0, 187, 75);
    // we multiple our values by Math.PI/180 to convert degrees to radians
    conicGrad2.addColorStop(0, 'black');
    conicGrad2.addColorStop(0.25, 'black');
    conicGrad2.addColorStop(0.25, 'white');
    conicGrad2.addColorStop(0.5, 'white');
    conicGrad2.addColorStop(0.5, 'black');
    conicGrad2.addColorStop(0.75, 'black');
    conicGrad2.addColorStop(0.75, 'white');
    conicGrad2.addColorStop(1, 'white');

    // draw shapes
    ctx.fillStyle = conicGrad1;
    ctx.fillRect(12, 25, 100, 100);
    ctx.fillStyle = conicGrad2;
    ctx.fillRect(137, 25, 100, 100);

}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Applying_styles_and_colors#a_createconicgradient_example"