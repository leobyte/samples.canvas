const frame = document.$("#frame").frame;
function appendImags(){
  let testelm = frame.document.$("#sciter_test");
  /*NOTE: can't use display:none */
  if(!frame.document.$("#imgs"))
    testelm.append(<div #imgs style="visibility: hidden;">
                  <img id="source" src={__DIR__+"rhino.jpg"} width="300" height="227" />
                  <img id="frame" src="./tutorial/04.Using images/canvas_picture_frame.png" width="132" height="150" />
                </div>)
  
}

export function draw(ctx) {
  // add img element
  appendImags();

  let img_source = frame.document.getElementById('source');
  let img_frame = frame.document.getElementById('frame');

  const [w1,h1] = img_source.state.box("dimension");
  const [w2,h2] = img_frame.state.box("dimension");

  // Draw slice
  ctx.drawImage(new Graphics.Image(w1, h1, img_source), 33, 71, 104, 124, 21, 20, 87, 104);

  // Draw frame
  ctx.drawImage(new Graphics.Image(w2,h2,img_frame), 0, 0);

}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#example_framing_an_image"