export function draw(ctx) {
 /* MDN
  const img = new Image();
  img.onload = () => {
    ctx.drawImage(img, 0, 0);
    ctx.beginPath();
    ctx.moveTo(30, 96);
    ctx.lineTo(70, 66);
    ctx.lineTo(103, 76);
    ctx.lineTo(170, 15);
    ctx.stroke();
  };
  img.src = 'backdrop.png';
*/
  
  /* sciter*/ 
  Graphics.Image.load(__DIR__+"/backdrop.png").then(img=>{
    ctx.drawImage(img, 0, 0);
    //ctx.draw(img, 0, 0);
    ctx.beginPath();
    ctx.moveTo(30, 96);
    ctx.lineTo(70, 66);
    ctx.lineTo(103, 76);
    ctx.lineTo(170, 15);
    ctx.stroke();
  })
}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#example_a_simple_line_graph"