export function draw(ctx) {
  
  ctx.font = "28px serif";
  ctx.textBaseline = "hanging";
  ctx.strokeText("Hello world", 0, 100);

}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_text#a_textbaseline_example"