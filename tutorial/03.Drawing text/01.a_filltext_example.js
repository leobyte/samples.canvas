export function draw(ctx) {
  
  ctx.font = '28px serif';
  ctx.fillText('Hello world', 10, 50);

}

draw.href = "https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_text#a_filltext_example"