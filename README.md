Download and unzip to the same level directory of `samples.charts`

 The following examples in `Sciter` are special:

# 02.Applying styles and colors

> 03.a_globalalpha_example

Transparency does not stack

> 08.a_demo_of_the_miterlimit_property

miter has different effect

> 09.using_line_dashes

no `lineDashOffset` function

>	12.a_createconicgradient_example 

no `createConicGradient` function
>	13.a_createpattern_example 

no `createPattern` function 
>	14.a_shadowed_text_example
no `shadow` properties 

# 03.Drawing text
> 02.a_stroketext_example

no `strokeText` function
> 03.a_textbaseline_example

no `strokeText` function

# 04.Using images	
> 03.example_framing_an_image

`drawImage` maybe has different units
> 04.art_gallery_example

`drawImage` maybe has different units
	
# 06.Compositing and clipping
> 01.Compositing_Example.htm

no `globalCompositeOperation` property

# 07.Basic animations
> 01.an_animated_solar_system

no `globalCompositeOperation` property

> 04.mouse_following_animation.htm

There are traces in the background, maybe the `rgba` has diffenent effect.

# 08.Advanced animationsns
> 04.fourth_demo.htm

The mouse coordinates are not in the ball's center

# 09.Pixel manipulation
> 01.Pixel_manipulation_with_canvas.htm

> 02.grayscaling_and_inverting_colors.htm

> 03.zooming_and_anti-aliasing.htm

some functions and properties are non-existent